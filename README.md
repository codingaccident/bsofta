 This repository holds the Backspace Sixty Five O Two Assembler. It is built to assemble 65XX assembly to bytecode, hex, arduino eeprom writer commands, and side by side outputs.

 Current list of Supported CPUs:
 65c816

 List of CPUS by priority that will be supported:
 65c02
 NMOS 6502
 65265
 65134

 This project is packaged under GPL V2.

 Binaries will be released periodically as jar files. Feel free to contribute to the wiki or open issues.

