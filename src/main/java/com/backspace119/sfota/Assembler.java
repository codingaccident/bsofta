package com.backspace119.sfota;

import com.backspace119.sfota.Utils.Utils;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.backspace119.sfota.Utils.MathUtils.ALL_MATH;
import static com.backspace119.sfota.Utils.MathUtils.fullMathParse;
import static com.backspace119.sfota.Utils.MathUtils.parenthesesMath;

public class Assembler {

    private static final String HEX_PATTERN = "(((0x)|\\$)[0-9a-fA-F]+)|(\\b([0-9a-fA-F]+[hH]))";
    private static final String OCTAL_PATTERN = "(([oO][0-7]+)|([0-7]+[oO]))";
    private static final String BINARY_PATTERN = "((0[Bb])[01]+\\b)|(\\b[01]+[Bb])";
    private static final String CHAR_PATTERN = "('.')";
    private static final String STRING_PATTERN = "(\"(.)+\")";
    private int DP_START = 0;
    private int S_END = 0x1FF;
    private int MEM_START = 0X200;
    private int MEM_END = 0x7FFA;
    private boolean MEM_EXTENSION = false;
    private int MEM_EXTENSION_START = 0X800000;
    private int MEM_EXTENSION_END = 0xFFFFFF;
    private int EEPROM_SIZE = 0x3FFF;
    private int EEPROM_START = 0xC000;
    private int EEPROM_PROGRAM_START = 0x4000;
    private int EEPROM_CONSTANT_START = 0xF000;
    private int EEPROM_CONSTANT_SPACE = 0xF00;
    private boolean USING_CONSTANT_SPACE = true;
    private boolean USING_EEPROM = true;
    private int currentDPPointer = 0;
    private int currentMemPointer = 0;
    private int currentConstPointer = 0;
    private int currentMemExtensionPointer = 0;

    public Assembler() {

    }

    public Assembler(int DP_START, int s_END, int MEM_START, int MEM_END, boolean MEM_EXTENSION, int MEM_EXTENSION_START, int MEM_EXTENSION_END, int EEPROM_SIZE, int EEPROM_START, int EEPROM_PROGRAM_START, boolean USING_EEPROM, int EEPROM_CONSTANT_START, int EEPROM_CONSTANT_SPACE, boolean USING_CONSTANT_SPACE) {
        this.DP_START = DP_START;
        S_END = s_END;
        this.MEM_START = MEM_START;
        this.MEM_END = MEM_END;
        this.MEM_EXTENSION = MEM_EXTENSION;
        this.MEM_EXTENSION_START = MEM_EXTENSION_START;
        this.MEM_EXTENSION_END = MEM_EXTENSION_END;
        this.EEPROM_SIZE = EEPROM_SIZE;
        this.EEPROM_START = EEPROM_START;
        this.EEPROM_PROGRAM_START = EEPROM_PROGRAM_START;
        this.USING_EEPROM = USING_EEPROM;
        this.EEPROM_CONSTANT_START = EEPROM_CONSTANT_START;
        this.EEPROM_CONSTANT_SPACE = EEPROM_CONSTANT_SPACE;
        this.USING_CONSTANT_SPACE = USING_CONSTANT_SPACE;
    }

    /**
     * This finds any "includes" statements and appends files in the correct places
     * @param input
     * @return
     */
    public List<String> stage0(List<String> input)
    {
        System.out.println("Parsing .includes directives and including files");
        int i = 0;
        List<String> output = new ArrayList<>();
        for(String line: input)
        {
            if(line.replaceAll(" ","").startsWith(".include"))
            {
                line = line.replaceFirst(".include","");
                Pattern string = Pattern.compile(STRING_PATTERN);
                Matcher mat = string.matcher(line);
                if(mat.find())
                {
                    String fileName = line.substring(mat.start(),mat.end());
                    fileName = fileName.replaceAll("\"","");
                    input.remove(i);
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
                        List<String> lines = br.lines().collect(Collectors.toList());
                        System.out.println("Adding: " + Arrays.toString(lines.toArray()) + " to input");
                        input.addAll(i,lines);
                        return stage0(input);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        throw new AssemblerException("Could not parse includes at line: " + (i + 1));
                    }

                }
            }
            i++;
            output.add(line);
        }
        System.out.println("Done.");
        return output;
    }
    /**
     * This takes all numbers within the given list of strings and converts them to decimal format
     * which is easier to work with in Java
     *
     * @return List of code with only numbers changed to decimal
     */
    public List<String> stage1(List<String> input) {
        System.out.println("Starting stage 1 of assembly: unify number literal format");
        //System.out.println("Stage1 input: " + Arrays.toString(input.toArray()));
        Pattern hex = Pattern.compile(HEX_PATTERN);
        Pattern bin = Pattern.compile(BINARY_PATTERN);
        Pattern oct = Pattern.compile(OCTAL_PATTERN);
        Pattern ch = Pattern.compile(CHAR_PATTERN);
        Pattern str = Pattern.compile(STRING_PATTERN);
        List<String> output = new ArrayList<>();
        for (String line : input) {
            if (line.startsWith(";")) continue;
            line = line.split(";")[0];
            Matcher hexMatcher = hex.matcher(line);
            Matcher binMatcher = bin.matcher(line);
            Matcher octMatcher = oct.matcher(line);
            Matcher chMatcher = ch.matcher(line);
            Matcher strMatcher = str.matcher(line);
            while (hexMatcher.find()) {
                String sub = line.substring(hexMatcher.start(), hexMatcher.end());
                String hexNumber = sub.replaceAll("\\$", "").replaceAll("[Hh]", "").replaceAll("0x", "");
                BigInteger bigInteger = new BigInteger(hexNumber, 16);
                //special case because of stupid regex
                if (sub.contains("$")) {
                    line = line.replaceFirst("\\$", "");
                    sub = sub.replaceFirst("\\$", "");
                }
                line = line.replaceFirst(sub, "" + bigInteger.intValue());
            }
            while (binMatcher.find()) {
                String sub = line.substring(binMatcher.start(), binMatcher.end());
                String binNumber = sub.replaceAll("0b", "").replaceAll("b", "");
                BigInteger bigInteger = new BigInteger(binNumber, 2);
                line = line.replaceFirst(sub, "" + bigInteger);
            }
            while (octMatcher.find()) {
                String sub = line.substring(octMatcher.start(), octMatcher.end());
                String octNumber = sub.replaceAll("[Oo]", "");
                BigInteger bigInteger = new BigInteger(octNumber, 8);
                line = line.replaceFirst(sub, "" + bigInteger);
            }
            while (chMatcher.find()) {
                String sub = line.substring(chMatcher.start(), chMatcher.end());
                String character = sub.replaceAll("'", "");
                try {
                    //System.out.println("Value of char: " + character + " is: " + character.getBytes("ASCII")[0]);
                    line = line.replaceFirst(sub, "" + character.getBytes("ASCII")[0]);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            while (strMatcher.find()) {
                String sub = line.substring(strMatcher.start(), strMatcher.end());
                String string = sub.replaceAll("\"", "");
                try {
                    BigInteger bigInt = new BigInteger(string.getBytes("ASCII"));
                    line = line.replaceFirst(sub, bigInt.toString(10));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            output.add(line);
        }
        System.out.println("Done.");
        return output;
    }

    /**
     * This takes all EQUates and distributes their number literals across the board. The numbers should already be replaced with decimal
     * which will make this a little bit easier. The hard part will be determining whether to do math here or later. Most likely
     * we'll leave that for the step after this one. This requires the user to place EQUates before they are used. This does not remove
     * EQU lines so we don't have problems with line offsets
     *
     * @param input
     * @return
     */
    public List<String> stage2(List<String> input) {
        System.out.println("Starting stage 2 of assembly: distribute EQU literals");
        List<String> output = new ArrayList<>();
        Map<String, String> equates = new HashMap<>();
        for (String line : input) {
            if (line.startsWith(";")) continue;
            line = line.split(";")[0];
            if (line.contains(" EQU ")) {
                line = line.replaceAll(" ", "");
                String[] split = line.split("EQU");
                String name = split[0].replaceAll("\\.", "");
                String equation = split[1];
                equates.put(name, equation);
                line = ";" + line;
            } else {
                for (Map.Entry<String, String> eq : equates.entrySet()) {
                    Pattern p = Pattern.compile("(\\b"+ eq.getKey() + "\\b)");
                    Matcher m = p.matcher(line);
                    while(m.find())
                    {
                        String l = line.substring(0,m.start());
                        l = l + eq.getValue();
                        line = l + line.substring(m.end());
                    }

                }

            }
            output.add(line);

        }
        System.out.println("Done.");
        return output;
    }

    /**
     * Takes all variable definitions and fits them to memory based on the setup of the assembler.
     * DDPS = Define Direct Page Storage
     * DFS = Define Storage (not direct page)
     *
     * @param input
     * @return
     */
    public List<String> stage3(List<String> input) throws AssemblerException {
        System.out.println("Starting stage 3 of assembly: map variables to memory and constants to constant space");
        Map<String, Integer> varAddressMap = new HashMap<>();
        Map<String, Integer> varLenMap = new HashMap<>();
        List<String> sortedVars = new ArrayList<>();
        List<String> output = new ArrayList<>();
        int i = 0;
        List<String> constantValues = new ArrayList<>();
        for (String line : input) {
            i++;
            if (line.startsWith(";")) continue;
            line = line.split(";")[0];
            //line = line.replaceAll(" ","");
            if (line.contains(" DDPS ")) {
                String[] split = line.split("DDPS");
                String var = split[0].replaceAll(" ", "");
                Integer size = Integer.parseInt(split[1].replaceAll(" ", "").split(";")[0]);
                if (size + currentDPPointer <= 255) {
                    varAddressMap.put(var, currentDPPointer);
                    varLenMap.put(var, size);
                    sortedVars.add(var);
                    currentDPPointer += size;
                } else {
                    throw new AssemblerException("Out of Direct Page memory at Line: " + i + " defining var: " + var + " of size: " + size + ". Resulting address would move out of DP");
                }
                continue;
            } else if (line.contains(" DFS ")) {
                String[] split = line.split("DFS");
                String var = split[0].replaceAll(" ", "");
                Integer size = Integer.parseInt(split[1].replaceAll(" ", "").split(";")[0]);
                if (size + currentMemPointer <= MEM_END - MEM_START) {
                    varAddressMap.put(var, currentMemPointer + MEM_START);
                    sortedVars.add(var);
                    varLenMap.put(var, size);
                    currentMemPointer += size;
                } else {
                    throw new AssemblerException("Out of memory at Line: " + i + " defining var: " + var + " of size: " + size + ". Resulting address would overflow memory");
                }
                continue;
            } else if (line.contains(" CONST ")) {
                if (USING_CONSTANT_SPACE) {
                    String[] split = line.split("CONST");
                    String cnst = split[0].replaceAll(" ", "");
                    BigInteger val = new BigInteger(split[1].replaceAll(" ", "").split(";")[0]);
                    int len = val.toByteArray().length;
                    if (len + currentConstPointer <= EEPROM_CONSTANT_SPACE) {
                        varAddressMap.put(cnst, currentConstPointer + EEPROM_CONSTANT_START);
                        sortedVars.add(cnst);
                        currentConstPointer += len;
                        varLenMap.put(cnst, len);
                        constantValues.add(val.toString(10));
                    } else {
                        throw new AssemblerException("Out of constant space at Line: " + i + " defining cnst: " + cnst + " of size: " + len + ". Resulting address would go past EEPROM constant space end");
                    }
                }
                continue;

            } else {
                //this is necessary so we check largest var names first, in case smaller var names are contained within the larger var name
                sortedVars.sort((a, b) -> (a.length() > b.length()) ? 1 : 0);
                for (String var : sortedVars) {
                    Pattern p = Pattern.compile("(\\b" + var + "\\b)");
                    Pattern len = Pattern.compile("(\\b" + var + "\\.len" + "\\b)" );
                    Matcher lMat = len.matcher(line);

                    //System.out.println("Searching var: " + var + " on line: " + line);
                    //System.out.println("Var: " + var + " Len: " + varLenMap.get(var));
                    while(lMat.find())
                    {
                        String l = line.substring(0,lMat.start());
                        l = l + varLenMap.get(var);
                        line = l + line.substring(lMat.end());
                    }
                    Matcher pMat = p.matcher(line);
                    while(pMat.find())
                    {
                        String l = line.substring(0,pMat.start());
                        l = l + varAddressMap.get(var);
                        line = l + line.substring(pMat.end());
                    }

                }
            }
            output.add(line);
        }
        if (USING_CONSTANT_SPACE) {
            output.add(".ORG " + EEPROM_CONSTANT_START);
            for (String cnst : constantValues) {
                output.add(".BYTES " + cnst);
            }
        }
        //System.out.println("Output of laying out vars: " + Arrays.toString(output.toArray()));
        System.out.println("Done.");
        return output;
    }

    private static final String MACRO_REGEX = "((.)+MACRO(.+)+)|((.)+MACRO)";

    /**
     * This finds all macro definitions and lays them out where they need to go.
     *
     * @param input
     * @return
     */
    public List<String> stage4(List<String> input) throws AssemblerException {
        System.out.println("Starting stage 4: expand macros");

        List<String> output = new ArrayList<>();

        Map<String, List<String>> macroMap = new HashMap<>();
        Map<String, List<String>> macroVars = new HashMap<>();
        for (int i = 0; i < input.size(); i++) {
            String line = input.get(i);
            if (line.startsWith(";")) continue;
            line = line.split(";")[0];
            Pattern mac = Pattern.compile(MACRO_REGEX);
            String find = line.replaceAll(" ", "");
            Matcher matcher = mac.matcher(find);

            if (matcher.find()) {

                System.out.println("Aggregating macro: " + line);
                String[] split = line.split("(\\bMACRO\\b)");
                String name = split[0].replaceAll(" ", "");
                String vars = "";
                List<String> varlist = null;
                if (split.length > 1) {
                    //System.out.println("Macro has vars");
                    vars = split[1];

                    varlist = new ArrayList<>(Arrays.asList(vars.split(" ")));
                    varlist.remove("");
                    varlist.remove(null);

                    System.out.println("Vars: " + vars);
                }
                //System.out.println("Found macro: " + name);
                int end = 0;
                List<String> macroLines = new ArrayList<>();
                for (int p = i + 1; p < input.size(); p++) {
                    if (input.get(p).startsWith("ENDM")) {
                        end = p;
                        break;
                    }
                    macroLines.add(input.get(p));
                }
                if (end == 0)
                    throw new AssemblerException("MACRO: " + name + " is missing ENDM to denote the end of the macro");
                macroMap.put(name, macroLines);
                if (varlist != null)
                    macroVars.put(name, varlist);
                i = end;

            }
        }
        output = layOutMacros(input, macroMap, macroVars,1);
        //System.out.print("State after macro expansion: " + Arrays.toString(output.toArray()));
        System.out.println("Done.");
        return output;
    }

    private List<String> layOutMacros(List<String> input, Map<String, List<String>> macroMap, Map<String, List<String>> macroVars, int iteration) {
        List<String> output = new ArrayList<>();
        int i = 0;
        boolean skipping = false;
        for (String line : input) {
            i++;
            if(line.contains("MACRO")) skipping = true;
            if(line.contains("ENDM")){
                skipping = false;
                continue;
            }
            if(skipping) continue;
            boolean dontAdd = false;
            for (Map.Entry<String, List<String>> macro : macroMap.entrySet()) {
                if (line.contains(macro.getKey())) {
                    //System.out.println("Found macro to extend: " + line);
                    dontAdd = true;
                    if (macroVars.get(macro.getKey()) != null && !macroVars.get(macro.getKey()).isEmpty()) {
                        while(line.contains("  ")) line = line.replaceAll("  "," ");
                        //System.out.println("Splitting line: " + line + " Macro: " + macro.getKey() + " vars: " + Arrays.toString(macroVars.get(macro.getKey()).toArray()));
                        line = line.replaceFirst(macro.getKey(), "").trim();

                        String[] split = line.split(" ");
                        if (split.length < macroVars.get(macro.getKey()).size()) {
                            System.out.println("Warning! Line " + i + " contains a usage of a macro without all variables present. Will attempt to compile by stopping at IFMA " + (split.length - 1) + " if IFMA was not used or was not used correctly, compilation will fail later as a result of this");

                        }
                        System.out.println("Line: " + line);
                        System.out.println("Macro: " + macro.getKey());
                        System.out.println("Macro Vars: " + Arrays.toString(macroVars.get(macro.getKey()).toArray()));
                        //System.out.println("Split: " + Arrays.toString(split));
                        List<String> modifiedLines = new ArrayList<>();
                        Map<String,String> modifiedLabels = new HashMap<>();
                        for (String l : macro.getValue()) {
                            if (l.contains("IFMA")) {
                                l = l.replaceFirst("IFMA", "");
                                int ifma = Integer.parseInt(l.replaceAll(" ", ""));
                                if (ifma == split.length - 1) break;
                                continue;
                            }
                            for (int p = 0; p < split.length; p++) {
                                //System.out.println(l);
                                l = l.replaceAll("\\b" + macroVars.get(macro.getKey()).get(p) + "\\b", " " + split[p]);
                            }
                            if(l.startsWith(".ML")){
                                String label = l.replaceFirst("\\.ML","").replaceAll(" ","").replace(":","");
                                l = l.replaceFirst("\\.ML", "").replaceAll(" ","").replaceAll(":","") + (i * iteration);
                                modifiedLabels.put(label,l);
                                l = l + ":";
                            }
                            modifiedLines.add(l);
                        }


                        for(int p = 0; p < modifiedLines.size(); p++)
                        {
                            String l = modifiedLines.get(p);
                            for(Map.Entry<String,String> label:modifiedLabels.entrySet()) {
                                if (l.contains(" " + label.getKey()))
                                {
                                    modifiedLines.set(p,l.replaceAll(label.getKey(),label.getValue()));
                                }
                            }
                        }
                        output.addAll(modifiedLines);
                    } else {
                        Map<String,String> modifiedLabels = new HashMap<>();
                        List<String> modifiedLines = new ArrayList<>();
                        for(String l:macro.getValue())
                        {
                            if(l.startsWith(".ML")){
                                String label = l.replaceFirst("\\.ML","").replaceAll(" ","").replace(":","");
                                l = l.replaceFirst("\\.ML", "").replaceAll(" ","").replaceAll(":","") + (i * iteration);
                                modifiedLabels.put(label,l);
                                l = l + ":";
                            }
                            modifiedLines.add(l);
                        }
                        for(int p = 0; p < modifiedLines.size(); p++)
                        {
                            String l = modifiedLines.get(p);
                            for(Map.Entry<String,String> label:modifiedLabels.entrySet()) {
                                if (l.contains(" " + label.getKey()))
                                {
                                    modifiedLines.set(p,l.replaceAll(label.getKey(),label.getValue()));
                                }
                            }
                        }
                        output.addAll(modifiedLines);

                    }
                }
            }
            if (!dontAdd)
                output.add(line);
        }
        boolean doExtra = false;
        for(String line: output)
        {
            for(Map.Entry<String,List<String>> macro:macroMap.entrySet()) {


                if (line.contains(macro.getKey())) {
                    doExtra = true;
                    break;
                }
            }
        }
        if(doExtra) return layOutMacros(output,macroMap,macroVars,iteration+1);
        return output;
    }

    /**
     * This stage solves all math equations used in the source, and assembles them to number literals
     *
     * @param input
     * @return
     */
    public List<String> stage5(List<String> input) {
        System.out.println("Starting stage 5: parse math equations");
        System.out.println("Stage 5 input: " + Arrays.toString(input.toArray()));
        Pattern allMath = Pattern.compile(ALL_MATH);
        List<String> output = new ArrayList<>();
        for (String l : input) {
            String line = l.replaceAll(" ", "");
            Matcher allMathM = allMath.matcher(line);
            if (allMathM.find()) {
                //System.out.println("parsing math on line: " + line + " " + allMathM.start() + " " + allMathM.end());
                if (line.contains("(")) {
                    line = parenthesesMath(line);
                } else {
                    line = fullMathParse(line);
                }
                output.add(line);

            } else {
                output.add(l);
            }
        }
        System.out.println("Done.");
        return output;
    }


    private ProcessorInfo info;

    /**
     * This parses the input opcodes and operands to hex, for various purposes
     *
     * @param input
     * @return
     */
    public List<String> stage6(List<String> input) {
        System.out.println("Stage 6 input: " + Arrays.toString(input.toArray()));
        return stage6AOptional(null, input, false);
    }

    /**
     * This is used by stage 6, but can optionally be used to create a sameline hex file (easier to put code side by side)
     *
     * @param input
     * @param sameLine
     * @return
     */
    public List<String> stage6AOptional(List<String> originalInput, final List<String> input, boolean sameLine) {
        System.out.println("Starting stage 6 " + (sameLine ? " with side by side compilation" : "") + ": compile opcodes to hex");
        //System.out.println("Stage 6 input: " + Arrays.toString(input.toArray()));
        info = new ProcessorInfo();
        int offset = 0;
        if (USING_EEPROM) {
            offset = EEPROM_START;
            info.setCurrentAddress(EEPROM_START);
        }
        ArrayList<String> output = new ArrayList<>((USING_EEPROM) ? EEPROM_SIZE : 0xFFFFFF);
        if (!sameLine)
            for (int i = 0; i < ((USING_EEPROM) ? EEPROM_SIZE : 0xFFFFFF); i++)
                output.add("EA");
        info.setCurrentLine(-1);
        for (String line : input) {
            //System.out.println("Input line: " + line);
            info.setCurrentLine(info.getCurrentLine() + 1);
            if (line.startsWith(".")) {
                //System.out.println("Line starts with . ");
                line = line.replaceFirst("\\.", "");
                byte status = info.getStatus();
                String check = line.split(" ")[0];
                switch (check) {
                    case "m8":
                        if ((status & (byte) 0b00100000) != 0b00100000) {
                            status = (byte) (status | (byte) 0b00100000);
                            info.setStatus(status);
                        }
                        break;
                    case "m16":
                        if ((status & (byte) 0b00100000) == 0b00100000) {
                            status = (byte) (status & (byte) 0b11011111);
                            info.setStatus(status);
                        }
                        break;
                    case "x8":
                        if ((status & (byte) 0b00010000) != 0b00010000) {
                            status = (byte) (status | (byte) 0b00010000);
                            info.setStatus(status);
                        }
                        break;
                    case "x16":
                        if ((status & (byte) 0b00010000) == 0b00010000) {
                            status = (byte) (status & (byte) 0b111011111);
                            info.setStatus(status);
                        }
                        break;
                    case "ORG":
                    case "org":
                        //System.out.println("Found org line");
                        if (!sameLine)
                            output.set(info.getCurrentAddress() - offset, "SKIP");
                        info.setCurrentAddress(Integer.parseInt(line.replaceAll("(ORG|org)", "").replaceAll(" ", "").split(";")[0]));
                        if (!(info.getCurrentAddress() - offset - 1 < 0))
                            if (!sameLine)
                                output.set(info.getCurrentAddress() - offset - 1, "ENDSKIP");
                        break;
                    case "BYTES":
                        //System.out.println("FOUND BYTES LINE: " + line);
                        String data = line.replaceFirst("BYTES", "").replaceAll(" ", "");
                        if (data.contains("\"")) {
                            data = data.replaceAll("\"", "");
                            try {
                                BigInteger val = new BigInteger(data.getBytes("ASCII"));
                                data = val.toString(10);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        BigInteger bigInt = new BigInteger(data, 10);
                        if (!sameLine) {
                            for (byte b : bigInt.toByteArray()) {
                                output.set(info.getCurrentAddress() - offset, String.format("%02x", b));
                                info.setCurrentAddress(info.getCurrentAddress() + 1);
                            }
                        } else {
                            output.add(Integer.toHexString(info.getCurrentAddress()) + " " + Utils.toHexArray(bigInt.toByteArray()) + "\t\t" + line);
                            info.setCurrentAddress(info.getCurrentAddress() + bigInt.toByteArray().length);
                        }
                        break;
                }
                if (sameLine) output.add("");
                continue;
            }
            if (line.startsWith(";")) {
                if (sameLine) output.add("");
                continue;
            }
            if(line.isEmpty()){
                if (sameLine) output.add("");
                continue;
            }
            line = line.replaceAll("\\*", "" + info.getCurrentAddress());
            //TODO add math parsing for anything that was using *
            //System.out.println("Xbit set: " + (((byte)0b00010000 & info.getStatus()) == (byte)0b00010000));
            //System.out.println(line);
            String opcode = line.replaceAll(" ", "").substring(0, 3);
            //System.out.println(opcode);
            //Opcodes o = Opcodes.valueOf("LDA");

            try {
                Opcodes op = Opcodes.valueOf(opcode.toUpperCase());
                byte[] out = op.getCompiler().compile(line.substring(3).replaceAll(" ", "").split(";")[0], info);
                if (out == null) {
                    throw new AssemblerException("Error parsing line: " + line);
                }
                output.ensureCapacity(info.getCurrentAddress() + out.length);
                if (!sameLine) {
                    int i = 0;
                    for (byte b : out) {
                        if (output.size() <= i + (info.getCurrentAddress()) - offset)
                            output.add(String.format("%02x", b));
                        else
                            output.set(i + (info.getCurrentAddress() - offset), String.format("%02x", b));
                        i++;
                    }
                } else {
                    output.add(Integer.toHexString(info.getCurrentAddress()).toUpperCase() + " " + Utils.toHexArray(out) + "\t\t" + line);
                }
                info.setCurrentAddress(info.getCurrentAddress() + out.length);
            } catch (IllegalArgumentException e) {
                //e.printStackTrace();
                //System.out.println("Creating label: " + line.split(" ")[0]);
                String label = line.split(" ")[0].replaceAll(":", "").replaceAll(" ", "");
                info.setSubroutineAddress(label);
                //System.out.println("Found label: " + label + " at address: " + String.format("%02x",info.getCurrentAddress()));

                if (info.shouldRevisit(label)) {
                    //System.out.println("Returning to line: " + input.get(info.getRevisitLine(label)) + " for label: " + label);
                    //System.out.println("Revisiting on this label");
                    List<Integer> revisitLines = info.getRevisitLine(label);
                    List<Integer> revisitAddresses = info.getRevisitAddress(label);
                    for (int i = 0; i < revisitLines.size(); i++) {
                        //System.out.println("Revisiting line: " + revisitLines.get(i) + " for label: " + label + " with address: " + revisitAddresses.get(i));
                        int currentAddress = info.getCurrentAddress();
                        info.setCurrentAddress(revisitAddresses.get(i));
                        Opcodes op = Opcodes.valueOf(input.get(revisitLines.get(i)).substring(0, 3));
                        byte[] out = op.getCompiler().compile(input.get(revisitLines.get(i)).substring(3).replaceAll(" ", "").split(";")[0], info);
                        if (!sameLine) {
                            int p = 0;

                            for (byte b : out) {
                                output.set(revisitAddresses.get(i) - offset + p, String.format("%02x", b));
                                p++;
                            }
                        } else {
                            output.set(revisitLines.get(i), Integer.toHexString(info.getCurrentAddress()).toUpperCase() + " " + Utils.toHexArray(out) + "\t\t" + input.get(revisitLines.get(i)));
                        }
                        info.setCurrentAddress(currentAddress);
                    }
                }

                if (sameLine) output.add(label);
            } catch (IndexOutOfBoundsException e1) {
                e1.printStackTrace();
                throw new AssemblerException("Ran out of EEPROM space (" + EEPROM_SIZE + ") bytes, or program is greater than 16M!");
            }


        }
        //System.out.println("Output from hex gen: " + Arrays.toString(output.toArray()));
        System.out.println("Done.");
        return output;
    }

    /**
     * This optional stage will output a list of strings in a format that can be read by my eeprom programmer built off a mega.
     * I may add support for others built off megas. This will be a list of commands like:
     * W 0x4000 0xEA that will write EA to address 4000
     * If padding is set to true all unwritten bytes will be EA (NOP). I may add support in the future for 00 (BRK)
     *
     * @param input
     * @param padding
     * @return
     */
    public List<String> stage7Optional(List<String> input, boolean padding) {
        System.out.println("Starting stage 7: compile Mega based EEPROM programmer command file");
        List<String> output = new ArrayList<>();
        int i = 0;
        System.out.println(Integer.toHexString(info.getCurrentAddress()));
        boolean skipping = false;
        for (String line : input) {
            if (line.equals("SKIP")) skipping = true;
            if (line.equals("ENDSKIP")) {
                skipping = false;
                continue;
            }
            if (skipping && !padding) {
                //System.out.println("Skipping: " + Integer.toHexString(i + EEPROM_START));
                i++;
                continue;
            }
            if (i >= info.getCurrentAddress() - ((USING_EEPROM) ? EEPROM_START : 0) && !padding) return output;
            output.add("W " + Integer.toHexString(i + EEPROM_PROGRAM_START) + " " + line);
            i++;
        }
        System.out.println("Done.");
        return output;
    }

    /**
     * This optional stage will output a side by side list of code to laid down hex.
     *
     * @param input
     * @return
     */
    @Deprecated
    public List<String> stage8Optional(List<String> input, List<String> sameLineHex) {
        List<String> output = new ArrayList<>();
        int i = 0;
        for (String line : sameLineHex) {
            if (input.size() > i)
                output.add(line + "    " + input.get(i));
            else break;
            i++;
        }
        return output;
    }

    /**
     * This final stage converts the hex to a raw binary byte array for output
     *
     * @param input
     * @return
     */
    public byte[] stage9(List<String> input) {
        System.out.println("Starting final stage: export binary");
        byte[] output = new byte[input.size()];
        int i = 0;
        for (String line : input) {
            //TODO possibly just skip here
            if(line.equals("SKIP") || line.equals("ENDSKIP")) line = "EA";
            byte b = (byte) (0xFF & Integer.parseInt(line, 16));
            output[i] = b;
            i++;
        }
        System.out.println("Done.");
        return output;
    }


}
