package com.backspace119.sfota;

public class AssemblerException extends RuntimeException {
    public AssemblerException(String message)
    {
        super(message);
    }
}
