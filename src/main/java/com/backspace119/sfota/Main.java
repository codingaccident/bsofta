package com.backspace119.sfota;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortException;
import jssc.SerialPortList;
import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * BSFOTA Backspace Sixty Five O Two Assembler, built for easy, modern computer compatible 65xx assembly
 * Copyright (C) 2019  Bryan Sharpe
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * For questions on licensing or concerns contact Bryan Sharpe at backspace119rd@gmail.com
 */
public class Main {


    private static int DP_START = 0;
    private static int S_END = 0x1FF;
    private static int MEM_START = 0X200;
    private static int MEM_END = 0x7FFA;
    private static boolean MEM_EXTENSION = false;
    private static int MEM_EXTENSION_START = 0X800000;
    private static int MEM_EXTENSION_END = 0xFFFFFF;
    private static int EEPROM_SIZE = 0x3FFF;
    private static int EEPROM_START = 0xC000;
    private static int EEPROM_PROGRAM_START = 0x4000;
    private static boolean USING_EEPROM = false;
    private static boolean USE_PADDING = false;
    private static boolean OUTPUT_HEX = false;
    private static boolean OUTPUT_WRITER = false;
    private static boolean OUTPUT_SBS = false;
    private static int EEPROM_CONSTANT_START = 0xF000;
    private static int EEPROM_CONSTANT_SPACE = 0xF00;
    private static boolean USING_CONSTANT_SPACE = true;
    private static String SERIAL_PORT = "COM7";
    private static String inputFile;
    private static String outputFile;

    private static void parseArgs(String[] args)
    {
        int i = 0;
        boolean skipNext = false;
        for(String arg:args)
        {
            if(skipNext) {
                skipNext = false;
                i++;
                continue;
            }
            switch(arg) {
                case "-i":
                    inputFile = args[i + 1];
                    skipNext = true;
                    break;
                case "-o":
                    outputFile = args[i + 1];
                    skipNext = true;
                    break;
                case "-h":
                    ClassLoader cl = ClassLoader.getSystemClassLoader();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(cl.getResourceAsStream("help.txt")));//must use stream to not break shit
                    reader.lines().forEach(System.out::println);
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
                    break;
                case "-man":
                    cl = ClassLoader.getSystemClassLoader();
                    reader = new BufferedReader(new InputStreamReader(cl.getResourceAsStream("man.txt")));//must use stream to not break shit
                    reader.lines().forEach(System.out::println);
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
                    break;
                case "-l":
                    cl = ClassLoader.getSystemClassLoader();
                    reader = new BufferedReader(new InputStreamReader(cl.getResourceAsStream("license.txt")));//must use stream to not break shit
                    reader.lines().forEach(System.out::println);
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
                    break;
                case "-e":
                    USING_EEPROM = true;
                    break;
                case "-es":
                    EEPROM_START = Integer.parseInt(args[i+1],16);
                    break;
                case "-eb":
                    EEPROM_START = Integer.parseInt(args[i+1],16);
                    break;
                case "-ep":
                    EEPROM_PROGRAM_START = Integer.parseInt(args[i+1],16);
                    break;
                case "-p":
                    USE_PADDING = true;
                    break;
                case "-hx":
                    OUTPUT_HEX = true;
                    break;
                case "-w":
                    OUTPUT_WRITER = true;
                    SERIAL_PORT = args[i+1];
                    skipNext = true;
                    break;
                case "-ms":
                    MEM_START = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-me":
                    MEM_END = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-dp":
                    DP_START = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-se":
                    S_END = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-mes":
                    MEM_EXTENSION = true;
                    MEM_EXTENSION_START = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-mee":
                    MEM_EXTENSION_END = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-sbs":
                    OUTPUT_SBS = true;
                    break;
                case "-eca":
                    EEPROM_CONSTANT_START = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-ecs":
                    EEPROM_CONSTANT_SPACE = Integer.parseInt(args[i+1],16);
                    skipNext = true;
                    break;
                case "-dec":
                    USING_CONSTANT_SPACE = false;
                    break;
                default:
                    System.out.println("Unrecognized switch: " + arg);
            }
            i++;
        }
    }


    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if(args.length == 0 || args[0].equals("-h"))
        {
            ClassLoader cl = ClassLoader.getSystemClassLoader();
            BufferedReader reader = new BufferedReader(new InputStreamReader(cl.getResourceAsStream("help.txt")));//must use stream to not break shit
            reader.lines().forEach(System.out::println);
            return;
        }
        parseArgs(args);
        if(inputFile == null || outputFile == null)
        {
            System.out.println("NO INPUT OR OUTPUT FILE NAME GIVEN. EXITING");
            System.exit(0);
        }

        Assembler ass = new Assembler(DP_START,S_END,MEM_START,MEM_END,MEM_EXTENSION,MEM_EXTENSION_START,MEM_EXTENSION_END,EEPROM_SIZE,EEPROM_START,EEPROM_PROGRAM_START,USING_EEPROM,EEPROM_CONSTANT_START,EEPROM_CONSTANT_SPACE,USING_CONSTANT_SPACE);

        try{
            BufferedReader br = new BufferedReader(new FileReader(new File(inputFile)));
            List<String> input;
            input = br.lines().collect(Collectors.toList());
            List<String> parsedinput = ass.stage5(ass.stage4(ass.stage3(ass.stage2(ass.stage1(ass.stage0(input))))));
            List<String> hex = ass.stage6(parsedinput);

            if(OUTPUT_WRITER)
            {
                List<String> writer = ass.stage7Optional(hex,USE_PADDING);
                PrintWriter bw = new PrintWriter(new FileWriter(new File(outputFile + "writer.txt")));
                for(String line:writer)
                bw.println(line);
                bw.flush();
                bw.close();
                System.out.println("===BEGIN SERIAL COMMUNICATION WITH AMEGA PROGRAMMER===");
                try {
                    SerialPort port = new SerialPort(SERIAL_PORT);
                    port.openPort();
                    port.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                    port.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);


                    port.setEventsMask(SerialPort.MASK_RXCHAR);
                    port.addEventListener((SerialPortEvent e) -> {
                        if(e.isRXCHAR())
                        try {
                            System.out.print(port.readString());
                        } catch (SerialPortException e1) {
                            e1.printStackTrace();
                        }
                    });
                    Thread.sleep(2000);
                    System.out.println("Please insert chip securely into programming slot and enter any input to continue");
                    Scanner scan = new Scanner(System.in);
                    while(!scan.hasNext()) {}
                    for(String line:writer)
                    {
                        //System.out.println("Writing line: " + line);
                        line = line +"\n";
                        port.writeBytes(line.getBytes("ASCII"));
                        Thread.sleep(10);
                    }
                    Thread.sleep(500);
                    port.closePort();
                    System.out.println("END SERIAL COMMUNICATION");
                } catch (SerialPortException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
            if(OUTPUT_SBS)
            {
                //List<String> sameLineHex = ass.stage6AOptional(input,true);
                List<String> sbs = ass.stage6AOptional(input,parsedinput,true);
                PrintWriter bw = new PrintWriter(new FileWriter(new File(outputFile + "sbs.txt")));
                for(String line:sbs)
                    bw.println(line);
                bw.flush();
                bw.close();
            }
            if(OUTPUT_HEX)
            {
                PrintWriter bw = new PrintWriter(new FileWriter(new File(outputFile + "hex.txt")));
                for(String line:hex)
                    bw.println(line);
                bw.flush();
                bw.close();
            }
            byte[] output = ass.stage9(hex);
            FileOutputStream fos = new FileOutputStream(new File(outputFile + ".bin"));
            fos.write(output);
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
