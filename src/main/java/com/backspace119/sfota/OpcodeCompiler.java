package com.backspace119.sfota;
@FunctionalInterface
public interface OpcodeCompiler {

    byte[] compile(String line, ProcessorInfo p);

}
