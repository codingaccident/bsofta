package com.backspace119.sfota;

import com.backspace119.sfota.Utils.MathUtils;

import java.math.BigInteger;

class OpcodeCompilingCode {

    private static final int M_BIT = 5;
    private static final int X_BIT = 4;

    private static final String NUMBER = "(\\b((\\$[0-9A-Fa-f]+)|([0-9]+)|([0-1]+[bB]))\\b)";
    private static final String PDIRXP = "\\(" + NUMBER + ",[xX]\\)";
    private static final String PDIRPY = "\\(" + NUMBER + "\\),[yY]";
    private static final String PDIRSPY = "\\(" + NUMBER + ",S\\),[yY]";
    private static final String PDIRP = "\\(" + NUMBER + "\\)";
    private static final String STKS = NUMBER + ",S";

    static final OpcodeCompiler ADC = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0x61,line);
        }
        if(line.matches(STKS))
        {
            return standard2(0x63,line);
        }
        if(line.matches(NUMBER))
        {
            //TODO we may need to go based on length of the hex string, since $00XX or $0000XX will be direct paged, when the program might be trying to do absolute or long
            if(replaceHex(line) > 65535)
            {
                return standard4(0x6F,line);
            }
            else if(replaceHex(line) > 255)
            {
                return standard3(0x6D,line);
            }else {
                return standard2(0x65, line);
            }
        }
        if(line.matches("\\["+ NUMBER + "]"))
        {
            return standard2(0x67,line);
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0x69,line);
            }else {
                return standard3(0x69,line);
            }
        }
        if(line.matches(PDIRPY))
        {
            return standard2(0x71,line);
        }
        if(line.matches("\\(" + NUMBER + "\\)"))
        {
            return standard2(0x72,line);
        }
        if(line.matches("\\(" + STKS + "\\)[Yy]"))
        {
            return standard2(0x73,line);
        }
        if(line.matches(NUMBER + ",X"))
        {
            return standard2(0x75,line);
        }
        if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0x77,line);
        }
        if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0x79,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 65535) {
                return standard3(0x7D, line);
            }else{
                return standard4(0x7F,line);
            }
        }

        return null;

    };

    static final OpcodeCompiler SBC = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0xE1,line);
        }
        if(line.matches(STKS))
        {
            return standard2(0XE3,line);
        }
        if(line.matches(NUMBER))
        {
            //TODO we may need to go based on length of the hex string, since $00XX or $0000XX will be direct paged, when the program might be trying to do absolute or long
            if(replaceHex(line) > 65535)
            {
                return standard4(0xEF,line);
            }
            else if(replaceHex(line) > 255)
            {
                return standard3(0xED,line);
            }else {
                return standard2(0xE5, line);
            }
        }
        if(line.matches("\\["+ NUMBER + "]"))
        {
            return standard2(0xE7,line);
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0xE9,line);
            }else {
                return standard3(0xE9,line);
            }
        }
        if(line.matches(PDIRPY))
        {
            return standard2(0xF1,line);
        }
        if(line.matches("\\(" + NUMBER + "\\)"))
        {
            return standard2(0xF2,line);
        }
        if(line.matches("\\(" + STKS + "\\)[Yy]"))
        {
            return standard2(0xF3,line);
        }
        if(line.matches(NUMBER + ",X"))
        {
            return standard2(0xF5,line);
        }
        if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0xF7,line);
        }
        if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0xF9,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 65535) {
                return standard3(0xFD, line);
            }else{
                return standard4(0xFF,line);
            }
        }

        return null;

    };

    static final OpcodeCompiler CMP = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0xC1,line);
        }
        if(line.matches(STKS))
        {
            return standard2(0xC3,line);
        }
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 65535)
            {
                return standard4(0xCF,line);
            }else if(replaceHex(line) > 255)
            {
                return standard3(0xCD,line);
            }else {
                return standard2(0xC5, line);
            }
        }
        if(line.matches("\\[" + NUMBER + "]"))
        {
            return standard2(0xC7,line);
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0xC9,line);
            }else{
                return standard3(0xC9,line);
            }
        }
        if(line.matches(PDIRPY))
        {
            return standard2(0xD1,line);
        }
        if(line.matches("\\(" + NUMBER + "\\)"))
        {
            return standard2(0xD2,line);
        }
        if(line.matches("\\(" + STKS + "\\)[Yy]" ))
        {
            return standard2(0xD3,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            return standard2(0xD5,line);
        }
        if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0xD7,line);
        }
        if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0xD9,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 65535)
            {
                return standard4(0xDF,line);
            }else {
                return standard3(0xDD, line);
            }
        }
        return null;
    };

    static final OpcodeCompiler CPX = (line, p) -> {
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(X_BIT,p.getStatus()))
            {
                return standard2(0xE0,line);
            }else{
                return standard3(0xE0,line);
            }
        }
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255) {
                return standard3(0xEC, line);
            }else {
                return standard2(0xE4, line);
            }
        }
        return null;
    };

    static final OpcodeCompiler CPY = (line, p) -> {
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(X_BIT,p.getStatus()))
            {
                return standard2(0xC0,line);
            }else{
                return standard3(0xC0,line);
            }
        }
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255) {
                return standard3(0xCC, line);
            }else {
                return standard2(0xC4, line);
            }
        }
        return null;
    };

    static final OpcodeCompiler DEC = (line, p) -> {
        if(line.isEmpty())
        {
            return standard1(0x3A);
        }
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0xCE,line);
            }else {
                return standard2(0xC6, line);
            }
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0xDE,line);
            }else {
                return standard2(0xD6, line);
            }
        }

        return null;
    };

    static final OpcodeCompiler INC = (line, p) -> {
        if(line.isEmpty())
        {
            return standard1(0x1A);
        }
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0xEE,line);
            }else {
                return standard2(0xE6, line);
            }
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0xFE,line);
            }else {
                return standard2(0xF6, line);
            }
        }

        return null;
    };
    static final OpcodeCompiler INA = (line,p) ->  standard1(0x1A);

    static final OpcodeCompiler DEX = (line, p) -> {
        if(line.isEmpty())
        {
            return standard1(0xCA);
        }
        return null;
    };
    static final OpcodeCompiler DEY = (line, p) -> {
        if(line.isEmpty())
        {
            return standard1(0x88);
        }
        return null;
    };
    static final OpcodeCompiler INX = (line, p) -> {
        if(line.isEmpty())
        {
            return standard1(0xE8);
        }
        return null;
    };
    static final OpcodeCompiler INY = (line, p) -> {
        if(line.isEmpty())
        {
            return standard1(0xC8);
        }
        return null;
    };

    static final OpcodeCompiler AND = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0x21,line);
        }
        if(line.matches(STKS))
        {
            return standard2(0X23,line);
        }
        if(line.matches(NUMBER))
        {
            //TODO we may need to go based on length of the hex string, since $00XX or $0000XX will be direct paged, when the program might be trying to do absolute or long
            if(replaceHex(line) > 65535)
            {
                return standard4(0x2F,line);
            }
            else if(replaceHex(line) > 255)
            {
                return standard3(0x2D,line);
            }else {
                return standard2(0x25, line);
            }
        }
        if(line.matches("\\["+ NUMBER + "]"))
        {
            return standard2(0x27,line);
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0x29,line);
            }else {
                return standard3(0x29,line);
            }
        }
        if(line.matches(PDIRPY))
        {
            return standard2(0x31,line);
        }
        if(line.matches("\\(" + NUMBER + "\\)"))
        {
            return standard2(0x32,line);
        }
        if(line.matches("\\(" + STKS + "\\)[Yy]"))
        {
            return standard2(0x33,line);
        }
        if(line.matches(NUMBER + ",X"))
        {
            return standard2(0x35,line);
        }
        if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0x37,line);
        }
        if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0x39,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 65535) {
                return standard3(0x3D, line);
            }else{
                return standard4(0x3F,line);
            }
        }

        return null;

    };

    static final OpcodeCompiler EOR = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0x41,line);
        }
        if(line.matches(STKS))
        {
            return standard2(0X43,line);
        }
        if(line.matches(NUMBER))
        {
            //TODO we may need to go based on length of the hex string, since $00XX or $0000XX will be direct paged, when the program might be trying to do absolute or long
            if(replaceHex(line) > 65535)
            {
                return standard4(0x4F,line);
            }
            else if(replaceHex(line) > 255)
            {
                return standard3(0x4D,line);
            }else {
                return standard2(0x45, line);
            }
        }
        if(line.matches("\\["+ NUMBER + "]"))
        {
            return standard2(0x47,line);
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0x49,line);
            }else {
                return standard3(0x49,line);
            }
        }
        if(line.matches(PDIRPY))
        {
            return standard2(0x51,line);
        }
        if(line.matches("\\(" + NUMBER + "\\)"))
        {
            return standard2(0x52,line);
        }
        if(line.matches("\\(" + STKS + "\\)[Yy]"))
        {
            return standard2(0x53,line);
        }
        if(line.matches(NUMBER + ",X"))
        {
            return standard2(0x55,line);
        }
        if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0x57,line);
        }
        if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0x59,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 65535) {
                return standard3(0x5D, line);
            }else{
                return standard4(0x5F,line);
            }
        }

        return null;

    };

    static final OpcodeCompiler ORA = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0x01,line);
        }
        if(line.matches(STKS))
        {
            return standard2(0X03,line);
        }
        if(line.matches(NUMBER))
        {
            //TODO we may need to go based on length of the hex string, since $00XX or $0000XX will be direct paged, when the program might be trying to do absolute or long
            if(replaceHex(line) > 65535)
            {
                return standard4(0x0F,line);
            }
            else if(replaceHex(line) > 255)
            {
                return standard3(0x0D,line);
            }else {
                return standard2(0x05, line);
            }
        }
        if(line.matches("\\["+ NUMBER + "]"))
        {
            return standard2(0x07,line);
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0x09,line);
            }else {
                return standard3(0x09,line);
            }
        }
        if(line.matches(PDIRPY))
        {
            return standard2(0x11,line);
        }
        if(line.matches("\\(" + NUMBER + "\\)"))
        {
            return standard2(0x12,line);
        }
        if(line.matches("\\(" + STKS + "\\)[Yy]"))
        {
            return standard2(0x13,line);
        }
        if(line.matches(NUMBER + ",X"))
        {
            return standard2(0x15,line);
        }
        if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0x17,line);
        }
        if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0x19,line);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 65535) {
                return standard3(0x1D, line);
            }else{
                return standard4(0x1F,line);
            }
        }

        return null;

    };

    public static final OpcodeCompiler BIT = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x2C,line);
            }else{
                return standard2(0x24,line);
            }
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x3C,line);
            }else{
                return standard2(0x34,line);
            }
        }
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0x89,line);
            }else{
                return standard3(0x89,line);
            }
        }
        return null;
    };

    public static final OpcodeCompiler TRB = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x1C,line);
            }else{
                return standard2(0x14,line);
            }
        }
        return null;
    };
    public static final OpcodeCompiler TSB = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x0C,line);
            }else{
                return standard2(0x04,line);
            }
        }
        return null;
    };

    public static final OpcodeCompiler ASL = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x0E,line);
            }else{
                return standard2(0x06,line);
            }
        }
        if(line.isEmpty())
        {
            return standard1(0x0A);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x1E,line);
            }else{
                return standard2(0x16,line);
            }
        }
        return null;
    };
    public static final OpcodeCompiler LSR = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x4E,line);
            }else{
                return standard2(0x46,line);
            }
        }
        if(line.isEmpty())
        {
            return standard1(0x4A);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x5E,line);
            }else{
                return standard2(0x56,line);
            }
        }
        return null;
    };
    public static final OpcodeCompiler ROL = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x2E,line);
            }else{
                return standard2(0x26,line);
            }
        }
        if(line.isEmpty())
        {
            return standard1(0x2A);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x3E,line);
            }else{
                return standard2(0x36,line);
            }
        }
        return null;
    };
    public static final OpcodeCompiler ROR = (line, p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x6E,line);
            }else{
                return standard2(0x66,line);
            }
        }
        if(line.isEmpty())
        {
            return standard1(0x6A);
        }
        if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 255)
            {
                return standard3(0x7E,line);
            }else{
                return standard2(0x76,line);
            }
        }
        return null;
    };

    static byte[] standardJMPorBRA2(String line, ProcessorInfo p, int op)
    {
        String label = line.replaceAll(" ","").split("[;+\\-*/^%]")[0];
        if(p.hasSubroutineDeclaration(label)) {
            int address = 0;
            if(MathUtils.hasMath(line.replaceFirst(label,p.getSubroutineAddress(label) + "")))
            {
                if(line.contains("("))
                {
                    address = Integer.parseInt(MathUtils.parenthesesMath(line.replaceFirst(label,p.getSubroutineAddress(label) + "")));
                }else {
                    address = Integer.parseInt(MathUtils.fullMathParse(line.replaceFirst(label, p.getSubroutineAddress(label) + "")));
                }
            }else{
                address = p.getSubroutineAddress(label);
            }
            if(p.getCurrentAddress() + 129 >= address && p.getCurrentAddress() - 126 <= address)
                return standard2(op, "" + ((address - p.getCurrentAddress()) - 2));
            else
                //TODO report line number instead of address
                throw new AssemblerException("Label: " + label + " is greater than 129 bytes or less than -126 bytes from the current branch instruction at line: " + p.getCurrentLine());
        }
        else {
            p.addRevisitAddress(label);
            return standard2(op, "$00");
        }
    }

    static byte[] standardJMPorBRA3(String line, ProcessorInfo p, int op,boolean relative)
    {
        //System.out.println("Setting up 3 byte JMP or BRA instruction");
        String label = line.replaceAll(" ","").split("[;+\\-*/^%]")[0];

        if(p.hasSubroutineDeclaration(label)) {
            int address = 0;
            if(MathUtils.hasMath(line.replaceFirst(label,p.getSubroutineAddress(label) + "")))
            {
                if(line.contains("("))
                {
                    address = Integer.parseInt(MathUtils.parenthesesMath(line.replaceFirst(label,p.getSubroutineAddress(label) + "")));
                }else {
                    address = Integer.parseInt(MathUtils.fullMathParse(line.replaceFirst(label, p.getSubroutineAddress(label) + "")));
                }
            }else{
                address = p.getSubroutineAddress(label);
            }
            //System.out.println("Address we wish to go to: " + address);
            //System.out.println("Our address: " + p.getCurrentAddress());
            //System.out.println("Offset from address: " + (address - p.getCurrentAddress()));
            if(p.getCurrentAddress() + 32770 >= address && p.getCurrentAddress() - 32766 <= address) {
                if(relative)
                return standard3(op, "" + ((address - p.getCurrentAddress()) - 3));
                else
                    return standard3(op,"" + address);
            }
            else
                //TODO report line number instead of address
                throw new AssemblerException("Label: " + label + " is greater than 32770 bytes or less than -32766 bytes from the current branch (LONG) instruction at line: " + p.getCurrentLine());
        }
        else {
            p.addRevisitAddress(label);
            return standard3(op, "$00");
        }
    }
    static final OpcodeCompiler BCC = (line,p) -> standardJMPorBRA2(line,p,0x90);
    static final OpcodeCompiler BCS = (line,p) -> standardJMPorBRA2(line,p,0xB0);
    static final OpcodeCompiler BEQ = (line,p) -> standardJMPorBRA2(line,p,0xF0);
    static final OpcodeCompiler BMI = (line,p) -> standardJMPorBRA2(line,p,0x30);
    static final OpcodeCompiler BNE = (line,p) -> standardJMPorBRA2(line,p,0xD0);
    static final OpcodeCompiler BPL = (line,p) -> standardJMPorBRA2(line,p,0x10);
    static final OpcodeCompiler BRA = (line,p) -> standardJMPorBRA2(line,p,0x80);
    static final OpcodeCompiler BVC = (line,p) -> standardJMPorBRA2(line,p,0x50);
    static final OpcodeCompiler BVS = (line,p) -> standardJMPorBRA2(line,p,0x70);
    static final OpcodeCompiler BRL = (line,p) -> standardJMPorBRA3(line,p,0x82,true);

    //TODO support labels
    static final OpcodeCompiler JMP = (line, p) ->
    {
        if(line.matches(PDIRP))
        {
            return standard3(0x6C,line);
        }else if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 0xFFFF)
            {
                return standard4(0x5C,line);
            }else {
                return standard3(0x4C, line);
            }
        }else if(line.matches(PDIRXP)){
            return standard3(0x7C,line);
        }else if(line.matches("\\[" + NUMBER + "]"))
        {
            return standard3(0xDC,line);
        }
        return null;
    };
    //TODO this can cause an exception, since if the line is malformed it will break this, and rather than going to null we're returning a call to a method that may break. Look into this
    static final OpcodeCompiler JSL = (line, p) -> standard4(0x22,line);
    static final OpcodeCompiler JSR = (line,p) -> {
        if(line.matches(NUMBER))
        {
            return standard3(0x20,line);
        }else if(line.matches(PDIRXP))
        {
            return standard3(0xFC,line);
        }else{
            return standardJMPorBRA3(line,p,0x20,false);
        }
    };

    static final OpcodeCompiler RTL = (line,p) -> standard1(0x6B);
    static final OpcodeCompiler RTS = (line,p) -> standard1(0x60);
    static final OpcodeCompiler BRK = (line,p) -> standard2(0x00,"$00");
    static final OpcodeCompiler COP = (line,p) -> standard2(0x02,"$02");
    static final OpcodeCompiler RTI = (line,p) -> standard1(0x40);
    //TODO note, the following *COULD* be followed along with with the processor status byte but this could possibly fuck up compilation, so presently they do not, look into further
    static final OpcodeCompiler CLC = (line,p) -> standard1(0x18);
    static final OpcodeCompiler CLD = (line,p) -> standard1(0xD8);
    static final OpcodeCompiler CLI = (line,p) -> standard1(0x58);
    static final OpcodeCompiler CLV = (line,p) -> standard1(0xB8);
    static final OpcodeCompiler SEC = (line,p) -> standard1(0x38);
    static final OpcodeCompiler SED = (line,p) -> standard1(0xF8);
    static final OpcodeCompiler SEI = (line,p) -> standard1(0x78);
    static final OpcodeCompiler REP = (line,p) -> standard2(0xC2,line);
    static final OpcodeCompiler SEP = (line,p) -> standard2(0xE2,line);
    static final OpcodeCompiler LDA = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0xA1,line);
        }else if(line.matches(STKS))
        {
            return standard2(0xA3,line);
        }else if(line.matches(NUMBER))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8))
            {
                return standard2(0xA5,line);
            }else if(replaceHex(line) < 0xFFFF)
            {
                return standard3(0xAD,line);
            }else{
                return standard4(0xAF,line);
            }
        }else if(line.matches("#" + NUMBER)){
            if(bitcheck(M_BIT,p.getStatus()))
            {
                return standard2(0xA9,line);
            }else{
                return standard3(0xA9,line);
            }
        }else if(line.matches("\\[" + NUMBER + "]"))
        {
            return standard2(0xA7,line);
        }else if(line.matches(PDIRPY))
        {
            return standard2(0xB1,line);
        }else if(line.matches(PDIRP))
        {
            return standard2(0xB2,line);
        }else if(line.matches(PDIRSPY))
        {
            return standard2(0xB3,line);
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            return standard2(0xB5,line);
        }else if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0xB7,line);
        }else if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0xB9,line);
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 0xFFFF)
            {
                return standard4(0xBF,line);
            }else {
                return standard3(0xBD, line);
            }
        }
        return null;
    };
    static final OpcodeCompiler LDX = (line,p) -> {
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(X_BIT,p.getStatus()))
            {
                return standard2(0xA2,line);
            }else{
                return standard3(0xA2,line);
            }
        }else if(line.matches(NUMBER))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8))
            {
                return standard2(0xA6,line);
            }else {
                return standard3(0xAE,line);
            }
        }else if(line.matches(NUMBER + ",[Yy]"))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8)) {
                return standard2(0xB6, line);
            }else
                return standard3(0xBE,line);
        }
        return null;
    };

    static final OpcodeCompiler LDY = (line,p) -> {
        if(line.matches("#" + NUMBER))
        {
            if(bitcheck(X_BIT,p.getStatus()))
            {
                return standard2(0xA0,line);
            }else{
                return standard3(0xA0,line);
            }
        }else if(line.matches(NUMBER))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8))
            {
                return standard2(0xA4,line);
            }else {
                return standard3(0xAC,line);
            }
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8))
                return standard2(0xB4,line);
            else
                return standard3(0xBC,line);
        }
        return null;
    };

    static final OpcodeCompiler STA = (line,p) -> {
        if(line.matches(PDIRXP))
        {
            return standard2(0X81,line);
        }else if(line.matches(STKS))
        {
            return standard2(0x83,line);
        }else if(line.matches(NUMBER))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8))
            {
                return standard2(0x85,line);
            }else if(replaceHex(line) < 0xFFFF)
            {
                return standard3(0x8D,line);
            }else{
                return standard4(0x8F,line);
            }
        }else if(line.matches("\\[" + NUMBER + "]"))
        {
            return standard2(0x87,line);
        }else if(line.matches(PDIRPY))
        {
            return standard2(0x91,line);
        }else if(line.matches(PDIRP))
        {
            return standard2(0x92,line);
        }else if(line.matches(PDIRSPY))
        {
            return standard2(0x93,line);
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            return standard2(0x95,line);
        }else if(line.matches("\\[" + NUMBER + "],[Yy]"))
        {
            return standard2(0x97,line);
        }else if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard3(0x99,line);
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            if(replaceHex(line) > 0xFFFF)
            {
                return standard4(0x9F,line);
            }else {
                return standard3(0x9D, line);
            }
        }
        return null;
    };
    static final OpcodeCompiler STX = (line,p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 0xFF)
            {
                return standard2(0x86,line);
            }else {
                return standard3(0x8E,line);
            }
        }else if(line.matches(NUMBER + ",[Yy]"))
        {
            return standard2(0x96,line);
        }
        return null;
    };

    static final OpcodeCompiler STY = (line,p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 0xFF)
            {
                return standard2(0x84,line);
            }else {
                return standard3(0x8C,line);
            }
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            return standard2(0x94,line);
        }
        return null;
    };
    static final OpcodeCompiler STZ = (line,p) -> {
        if(line.matches(NUMBER))
        {
            if(replaceHex(line) > 0xFF)
            {
                return standard2(0x64,line);
            }else {
                return standard3(0x9C,line);
            }
        }else if(line.matches(NUMBER + ",[Xx]"))
        {
            if((replaceHex(line) & (p.getDP() << 8)) == (p.getDP() << 8))
                return standard2(0x74,line);
            else
                return standard3(0x9E,line);
        }
        return null;
    };
    static final OpcodeCompiler MVN = (line,p) -> standard32Args(0x54,line);
    static final OpcodeCompiler MVP = (line,p) -> standard32Args(0x44,line);
    static final OpcodeCompiler NOP = (line,p) -> standard1(0xEA);
    //TODO this has been used (in theory) as a 2 byte noop that could also jump the next instruction. Here, we are going to use it as it is intended to be used, but, this may change to allow the said "hack""
    static final OpcodeCompiler WDM = (line,p) -> standard2(0x54,"$EA");
    static final OpcodeCompiler PEA = (line,p) -> standard3(0XF4,line);
    static final OpcodeCompiler PEI = (line,p) -> standard2(0XD4,line);
    static final OpcodeCompiler PER = (line,p) -> {
        if(p.hasSubroutineDeclaration(line))
            return standard3(0XF4,"" + p.getSubroutineAddress(line));
        else {
            p.addRevisitAddress(line);
            return standard3(0xF4, "$00");
        }
    };
    static final OpcodeCompiler PHA = (line,p) -> standard1(0x48);
    static final OpcodeCompiler PHX = (line,p) -> standard1(0XDA);
    static final OpcodeCompiler PHY = (line,p) -> standard1(0x5A);
    static final OpcodeCompiler PLA = (line,p) -> standard1(0x68);
    static final OpcodeCompiler PLX = (line,p) -> standard1(0xFA);
    static final OpcodeCompiler PLY = (line,p) -> standard1(0x7A);

    static final OpcodeCompiler PHB = (line,p) -> standard1(0x8B);
    static final OpcodeCompiler PHD = (line,p) -> {
        p.push(p.getDP());
        return standard1(0x0B);
    };
    static final OpcodeCompiler PHK = (line,p) -> standard1(0x4B);
    static final OpcodeCompiler PHP = (line,p) -> standard1(0x08);
    static final OpcodeCompiler PLB = (line,p) -> standard1(0xAB);
    static final OpcodeCompiler PLD = (line,p) -> {
        p.setDP(p.pop());
        return standard1(0x2B);
    };
    static final OpcodeCompiler PLP = (line,p) -> standard1(0x28);

    static final OpcodeCompiler STP = (line,p) -> standard1(0XDB);
    static final OpcodeCompiler WAI = (line,p) -> standard1(0xCB);

    static final OpcodeCompiler TAX = (line,p) -> standard1(0xAA);
    static final OpcodeCompiler TAY = (line,p) -> standard1(0xA8);
    static final OpcodeCompiler TSX = (line,p) -> standard1(0xBA);
    static final OpcodeCompiler TXA = (line,p) -> standard1(0x8A);
    static final OpcodeCompiler TXS = (line,p) -> standard1(0x9A);
    static final OpcodeCompiler TXY = (line,p) -> standard1(0x9B);
    static final OpcodeCompiler TYA = (line,p) -> standard1(0x98);
    static final OpcodeCompiler TYX = (line,p) -> standard1(0xBB);

    static final OpcodeCompiler TCD = (line,p) -> {
        p.setDP(p.getC());
        return standard1(0x5B);
    };
    static final OpcodeCompiler TCS = (line,p) -> standard1(0x1B);
    static final OpcodeCompiler TDC = (line,p) -> standard1(0x7B);
    static final OpcodeCompiler TSC = (line,p) -> standard1(0x3B);

    static final OpcodeCompiler XBA = (line,p) -> standard1(0xEB);
    static final OpcodeCompiler XCE = (line,p) -> standard1(0xFB);


    private static byte[] standard32Args(int opcode, String line)
    {
        String[] split = line.split(",");
        byte[] out = new byte[3];
        out[0] = (byte)(0xFF & opcode);
        out[1] = (byte) replaceHex(split[0]);
        out[2] = (byte) replaceHex(split[1]);
        return out;
    }
    private static byte[] standard1(int opcode)
    {
        byte[] result = new byte[1];
        result[0] = (byte)opcode;
        return result;
    }
    private static byte[] standard2(int opcode, String line)
    {
        byte[] result = new byte[2];
        result[0] = (byte)opcode;
        int replaced = replaceHex(line);
        result[1] = ((byte) replaced);
        return result;
    }

    private static byte[] standard3(int opcode,String line)
    {
        byte[] result = new byte[3];
        result[0] = (byte)opcode;
        int val = replaceHex(line);
        boolean neg = false;
        if(val < 0) neg = true;
        //System.out.println("Integer: " + Integer.toHexString(val));
        result[1] = (byte) ((0xFF & val));
        result[2] = (byte) ((0xFF & (val >> 8)) | ((neg) ? 0b10000000:0));
        return result;
    }

    private static byte[] standard4(int opcode, String line)
    {
        byte[] result = new byte[4];
        result[0] = (byte)opcode;
        int val = replaceHex(line);
        boolean neg = false;
        if(val < 0) neg = true;
        result[1] = (byte) ((0xFF & val));
        result[2] = (byte) ((0xFF & (val >> 8)));
        result[3] = (byte) (0xFF & (val >> 16) | ((neg) ? 0b10000000:0));
        return result;
    }

    //only need 24 bits
    private static int replaceHex(String line)
    {
        boolean hex = false;
        boolean bin = false;
        if(line.contains("$"))
        {
            hex = true;
        }else if(line.toLowerCase().contains("b"))
        {
            bin = true;
        }
        if(bin) line = line.replaceAll("[Bb]","");
        //System.out.println("Number: " + line);
        int res = new BigInteger(line.replaceFirst("\\(","").replaceFirst("#","").replaceFirst("\\$","").split(",")[0].replaceAll("\\)",""),(hex) ? 16:(bin) ?  2: 10).intValue();
       return res;
    }

    private static boolean bitcheck(int bitnumber,int data)
    {
        return ((data & (1 << bitnumber)) != 0);
    }


}
