package com.backspace119.sfota;

import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.*;

public class ProcessorInfo {
    private byte status = (byte)0b00110000;
    private Map<String,Byte> statusToSubroutine = new HashMap<>();
    private Map<String,Integer> subRoutineAddresses = new HashMap<>();
    private Map<String,List<Integer>> revisitMap = new HashMap<>();
    private Map<String,List<Integer>> revisitLineMap = new HashMap<>();
    private int currentAddress = 0;
    private int currentLine = 0;
    private int dpLoc = 0;
    private int C = 0;//accumulator
    private Deque<Integer> stack = new ArrayDeque<>(256);

    public void setC(int c)
    {
        C = c;
    }
    public int getC()
    {
        return C;
    }
    public void push(int val)
    {
        stack.push(val);
    }
    public int pop()
    {
        return stack.pop();
    }
    public int getCurrentLine() {
        return currentLine;
    }

    public void setCurrentLine(int currentLine) {
        this.currentLine = currentLine;
    }

    public boolean shouldRevisit(String label)
    {
        return revisitMap.containsKey(label);
    }
    public List<Integer> getRevisitAddress(String label)
    {
        return revisitMap.get(label);
    }
    public void addRevisitAddress(String label)
    {
        if(!revisitMap.containsKey(label)) {
            revisitMap.put(label,new ArrayList<>());
            revisitLineMap.put(label,new ArrayList<>());
        }
        revisitMap.get(label).add(getCurrentAddress());
        revisitLineMap.get(label).add(getCurrentLine());

    }
    public List<Integer> getRevisitLine(String label)
    {
        return revisitLineMap.get(label);
    }
    public int getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(int currentAddress) {
        this.currentAddress = currentAddress;
    }

    public void setSubroutineAddressCustomAddress(String label,int address)
    {
        subRoutineAddresses.put(label,address);
    }
    public void addSubroutine(String subroutine)
    {
        statusToSubroutine.put(subroutine,status);
    }
    public boolean hasSubroutineDeclaration(String subroutine) { return subRoutineAddresses.containsKey(subroutine); }
    public int getSubroutineAddress(String subroutine)
    {
        return subRoutineAddresses.get(subroutine);
    }
    public void setSubroutineAddress(String subroutine)
    {
        subRoutineAddresses.put(subroutine,getCurrentAddress());
    }
    public boolean hasSubroutineCaller(String subroutine)
    {
        return statusToSubroutine.containsKey(subroutine);
    }
    public void loadStatus(String subroutine)
    {
        status = statusToSubroutine.remove(subroutine);
    }

    public void setStatus(byte status)
    {
        this.status = status;
    }

    public byte getStatus()
    {
        return status;
    }

    public void setDP(int dp)
    {
        dpLoc = dp;
    }

    public int getDP()
    {
        return dpLoc;
    }
}
