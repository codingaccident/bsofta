package com.backspace119.sfota.Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MathUtils {

    public static final String MATH = "(([+\\-*^%/])|(<<|>>))";
    public static final String DECIMAL = "(([0-9]+)|([0-9]+\\.[0-9]+))";
    public static final String ALL_MATH = "(" + DECIMAL + MATH + DECIMAL + ")+";

    public static boolean hasMath(String line)
    {
        Pattern allMath = Pattern.compile(ALL_MATH);
        return allMath.matcher(line).find();
    }
    /**
     * This takes a line like 1 + (2 + 3) and returns a literal
     *
     * @param line
     * @return
     */
    public static String parenthesesMath(String line) {
        StringBuilder sb = new StringBuilder();
        String sub = line.substring(line.lastIndexOf('('), line.indexOf(')'));
        String begin = line.substring(0, line.lastIndexOf('('));
        String end = line.substring(line.indexOf(')') + 1);
        String removedPar = sub.replaceAll("\\(", "").replaceAll("\\)", "");
        //System.out.println("Removed parentheses: " + removedPar);
        String parsed = fullMathParse(removedPar);
        sb.append(begin);
        sb.append(parsed);
        sb.append(end);
        line = sb.toString();
        //System.out.println(line);
        Pattern allMath = Pattern.compile(ALL_MATH);
        Matcher intermediate = allMath.matcher(line);
        if (line.contains("(")) {
            if (intermediate.find())
                return parenthesesMath(line);
        }

        if (intermediate.find()) {
            return fullMathParse(line);
        }
        return line;
    }

    /**
     * Takes a line like 1 + 3 + 2 + 4.5 and returns a literal
     *
     * @param line
     * @return
     */
    public static String fullMathParse(String line) {
        Pattern allMath = Pattern.compile(ALL_MATH);
        Matcher intermediate = allMath.matcher(line);
        if (intermediate.find()) {
            line = mathParse(line);
        }
        //System.out.println(line);
        intermediate = allMath.matcher(line);
        if (intermediate.find()) return fullMathParse(line);
        return line;
    }

    /**
     * takes string like 1 + 3 and returns literal
     *
     * @param line
     * @return
     */
    public static String mathParse(String line) {

        Pattern math = Pattern.compile(MATH);
        //Pattern dec = Pattern.compile(DECIMAL);
        Matcher mathM = math.matcher(line);

        StringBuilder sb = new StringBuilder();

        if (mathM.find()) {
            char op = line.charAt(mathM.start());
            String sub = line.substring(0, mathM.start()).replaceAll(" ", "");
            String sub2 = line.substring(mathM.start() + 1).replaceAll(" ", "");
            //System.out.println("Sub1: " + sub);
            //System.out.println("Sub2: " + sub2);
            int n1Begin = 0;
            for (int i = sub.length() - 1; i >= 0; i--) {
                if (!sub.substring(i).matches(DECIMAL)) {
                    n1Begin = i + 1;
                    break;
                }
            }
            int n2End = sub2.length();
            for (int i = 1; i < sub2.length(); i++) {
                if (!sub2.substring(0, i).matches(DECIMAL)) {
                    n2End = i - 1;
                    break;
                }
            }
            sb.append(line, 0, n1Begin);
            //System.out.println("N1: " + sub.substring(n1Begin, sub.length()));
            BigDecimal n1 = new BigDecimal(sub.substring(n1Begin, sub.length()));
            //System.out.println("N2: " + sub2.substring(0, n2End));
            BigDecimal n2 = new BigDecimal(sub2.substring(0, n2End));
            BigDecimal result = null;
            //TODO we are just shifting, not rotating
            switch (op) {
                case '+':
                    result = n1.add(n2);
                    break;
                case '-':
                    result = n1.subtract(n2);
                    break;
                case '*':
                    result = n1.multiply(n2);
                    break;
                case '%':
                    result = n1.divideAndRemainder(n2)[1];
                    break;
                case '/':
                    result = n1.divide(n2, RoundingMode.DOWN);
                    break;
                case '^':
                    result = n1.pow(n2.intValue());
                    break;
                case '>':
                    result = new BigDecimal(n1.unscaledValue().shiftRight(n2.intValue()),n1.scale());
                    break;
                case '<':
                    result = new BigDecimal(n1.unscaledValue().shiftLeft(n2.intValue()),n1.scale());
            }
            Objects.requireNonNull(result);
            //System.out.println("result of math: " + result.toPlainString());
            //System.out.println("SUB: " + line.substring(n2End));

            sb.append(result.toPlainString());
            sb.append(line.substring(line.length() - (sub2.length() - n2End)));
            return sb.toString();
        }
        return "";
    }
}
