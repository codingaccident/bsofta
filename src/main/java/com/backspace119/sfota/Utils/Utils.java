package com.backspace119.sfota.Utils;

public class Utils {
    public static String toHexArray(byte[] array)
    {
        StringBuilder sb = new StringBuilder();
        for(byte b:array)
        {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }
}
