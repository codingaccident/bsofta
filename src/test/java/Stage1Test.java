import com.backspace119.sfota.Assembler;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Stage1Test {

    @Test
    public void test()
    {
        List<String> input = new ArrayList<>();
        input.add("LDA #$FAAF");
        input.add("LDX o374");
        input.add("LDY 0b010101");
        input.add("LDA 0.123");
        Assembler ass = new Assembler();
        input = ass.stage1(input);
        System.out.println(Arrays.toString(input.toArray()));
    }
}
